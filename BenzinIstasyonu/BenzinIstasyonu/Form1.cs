﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BenzinIstasyonu
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //içerisinde akaryakıt türlerinin miktarlarını tutan classımız...
        public class Depo
        {
          public double Benzin95,
                   Benzin97,
                   Dizel,
                   EuroDizel,
                   Lpg;
        }
        //içerisinde akaryakıt türlerinin fiyatını tutan classımız...
        public class Fiyatlar
        {
          public double Benzin95,
                   Benzin97,
                   Dizel,
                   EuroDizel,
                   Lpg;
        }
        #region 
        //Sınıflarımızı Çağırdık
        Fiyatlar fiyat = new Fiyatlar();
        Depo depo = new Depo();

        //Eklenecek yakıt miktarlarını tutucak olan değişkenler
        double e_Benzin95 = 0,
               e_Benzin97 = 0,
               e_Dizel = 0,
               e_EuroDizel = 0,
               e_Lpg = 0;
      
        //Satış miktarını tutan değişkenler (Hangi yakıttan kaç litre satılacak?)
        double s_Benzin95 = 0,
               s_Benzin97 = 0,
               s_Dizel = 0,
               s_EuroDizel = 0,
               s_Lpg = 0;


        //Txt Dosyasını okuduktan sonra depo bilgisi satırlarını ve fiyat bilgisi satırlarını atamak için 2 dizi tanımladık.
        string[] depoBilgileri;
        string[] fiyatBilgileri;
        //Yakıt tiplerini tutan combobox için bir dizi tanımladık.
        string[] YakitTurleri = { "Benzin (95)", "Benzin (97)", "Dizel", "Euro Dizel", "LPG" };
        #endregion
        private void button6_Click(object sender, EventArgs e)
        {
            //satilacak miktarları numeric updown içerisinden değişkenlerimize aktardık.
            s_Benzin95 = double.Parse(bn95.Value.ToString());
            s_Benzin97 = double.Parse(bn97.Value.ToString());
            s_Dizel = double.Parse(bndizel.Value.ToString());
            s_EuroDizel = double.Parse(bnedizel.Value.ToString());
            s_Lpg = double.Parse(bnLPG.Value.ToString());
            //aktif olan numeric up down içerisindeki miktarı depodan düştük 
            //aynı zamanda ödenecek tutarı hesaplamak için fiyat classındaki ilgili yakıt fiyatındaki değerle satis miktari tutulan değişkenle çarpıldı
            if (bn95.Enabled==true)
            {
                depo.Benzin95 = depo.Benzin95 - s_Benzin95;
                tutar.Text = Convert.ToString(s_Benzin95 * fiyat.Benzin95) + " ₺"; 
            }
            else if (bn97.Enabled == true)
            {
                depo.Benzin97 = depo.Benzin97 - s_Benzin97;
                tutar.Text = Convert.ToString(s_Benzin97 * fiyat.Benzin97)+" ₺";
            }
            else if (bndizel.Enabled == true)
            {
                depo.Dizel= depo.Dizel - s_Dizel;
                tutar.Text = Convert.ToString(s_Dizel * fiyat.Dizel) + " ₺";
            }
            else if (bnedizel.Enabled == true)
            {
                depo.EuroDizel = depo.EuroDizel - s_EuroDizel;
                tutar.Text = Convert.ToString(s_EuroDizel * fiyat.EuroDizel) + " ₺";
            }
            else if (bnLPG.Enabled == true)
            {
                depo.Lpg = depo.Lpg - s_Lpg;
                tutar.Text = Convert.ToString(s_Lpg * fiyat.Lpg) + " ₺";
            }

            // Depo bilgileri yeni değerlerle güncellendi
            depoBilgileri[0] = Convert.ToString(depo.Benzin95);
            depoBilgileri[1] = Convert.ToString(depo.Benzin97);
            depoBilgileri[2] = Convert.ToString(depo.Dizel);
            depoBilgileri[3] = Convert.ToString(depo.EuroDizel);
            depoBilgileri[4] = Convert.ToString(depo.Lpg);
            //Güncel Değerler Tekrar Txt dosyasına yazıldı ve yeni değerler okundu.
            System.IO.File.WriteAllLines(Application.StartupPath + "\\depo.txt", depoBilgileri);
            txt_Depo_Oku();
            txt_Depo_Yaz();
            Progresbar_Guncelle();
            Numeric_Value();
            //Numeric updown nesneleri sıfırlandı
            bn95.Value = 0;
            bn97.Value = 0;
            bndizel.Value = 0;
            bnedizel.Value = 0;
            bnLPG.Value = 0;
            



        }

       

        private void cboxyt_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            //Seçilen yakıt türüne göre numeric updown nesneleri aktif/pasif edildi
            if (cboxyt.Text== "Benzin (95)")
            {
                bn95.Enabled = true;
                bn97.Enabled = false;
                bndizel.Enabled = false;
                bnedizel.Enabled = false;
                bnLPG.Enabled = false;
            }
            else if (cboxyt.Text == "Benzin (97)")
            {
                bn95.Enabled = false;
                bn97.Enabled = true;
                bndizel.Enabled = false;
                bnedizel.Enabled = false;
                bnLPG.Enabled = false;
            }
            else if (cboxyt.Text == "Dizel")
            {
                bn95.Enabled = false;
                bn97.Enabled = false;
                bndizel.Enabled = true;
                bnedizel.Enabled = false;
                bnLPG.Enabled = false;
            }
            else if(cboxyt.Text == "Euro Dizel")
            {
                bn95.Enabled = false;
                bn97.Enabled = false;
                bndizel.Enabled = false;
                bnedizel.Enabled = true;
                bnLPG.Enabled = false;
            }
            else if(cboxyt.Text == "LPG")
            {
                bn95.Enabled = false;
                bn97.Enabled = false;
                bndizel.Enabled = false;
                bnedizel.Enabled = false;
                bnLPG.Enabled = true;
            }
            bn95.Value = 0;
            bn97.Value = 0;
            bndizel.Value = 0;
            bnedizel.Value = 0;
            bnLPG.Value = 0;
            tutar.Text = "__________";
        }

      
    
        private void btnflpg_Click(object sender, EventArgs e)
        {
            //İlgili yakıt tipinin fiyatına % olarak zam/indirim yapılmasını sağladık
            try
            {
                fiyat.Benzin95 = fiyat.Benzin95 + (fiyat.Benzin95 * Convert.ToDouble(txtfb95.Text) / 100);
                fiyatBilgileri[0] = Convert.ToString(fiyat.Benzin95);
                
            }
            catch (Exception)
            {


            }
            try
            {
                fiyat.Benzin97 = fiyat.Benzin97 + (fiyat.Benzin97 * Convert.ToDouble(txtfb97.Text) / 100);
                fiyatBilgileri[1] = Convert.ToString(fiyat.Benzin97);

            }
            catch (Exception)
            {


            }
            try
            {
                fiyat.Dizel = fiyat.Dizel + (fiyat.Dizel * Convert.ToDouble(txtfdizel.Text) / 100);
                fiyatBilgileri[2] = Convert.ToString(fiyat.Dizel);

            }
            catch (Exception)
            {


            }
            try
            {
                fiyat.EuroDizel = fiyat.EuroDizel + (fiyat.EuroDizel * Convert.ToDouble(txtfedizel.Text) / 100);
                fiyatBilgileri[3] = Convert.ToString(fiyat.EuroDizel);

            }
            catch (Exception)
            {


            }
            try
            {
                fiyat.Lpg = fiyat.Lpg + (fiyat.Lpg * Convert.ToDouble(txtflpg.Text) / 100);
                fiyatBilgileri[4] = Convert.ToString(fiyat.Lpg);

            }
            catch (Exception)
            {


            }
            //Yeni fiyat bilgilerini txt Dosyasına yazdırdık ardından tekrar okuyup forma bilgileri yazdırdık
            System.IO.File.WriteAllLines(Application.StartupPath + "\\fiyat.txt", fiyatBilgileri);
            txt_fiyat_Oku();
            txt_fiyat_Yaz();
           
        }

        private void btnb95_Click(object sender, EventArgs e)
        {
            //Maximum depo kapasitesini aşmamak şartıyla textbox'a girilen yakıtı depomuza ekledik
            try
            {
                e_Benzin95 = Convert.ToDouble(txtbnz95.Text);
                if (1000<depo.Benzin95+e_Benzin95 ||e_Benzin95<=0 )
                {
                    txtbnz95.Text="Hata ! MAX(1000)";
                }
                else
                {
                    depoBilgileri[0] = Convert.ToString(depo.Benzin95 + e_Benzin95);
                }
            }
            catch (Exception)
            {

                
            }
            try
            {
                e_Benzin97 = Convert.ToDouble(txtbnz97.Text);
                if (1000 < depo.Benzin97 + e_Benzin97 || e_Benzin97 <= 0)
                {
                    txtbnz97.Text = "Hata ! MAX(1000)";
                }
                else
                {
                    depoBilgileri[1] = Convert.ToString(depo.Benzin97 + e_Benzin97);
                }
            }
            catch (Exception)
            {

                
            }
            try
            {
                e_Dizel = Convert.ToDouble(txtdizel.Text);
                if (1000 < depo.Dizel + e_Dizel || e_Dizel <= 0)
                {
                    txtdizel.Text="Hata ! MAX(1000)";
                }
                else
                {
                    depoBilgileri[2] = Convert.ToString(depo.Dizel + e_Dizel);
                }
            }
            catch (Exception)
            {

                
            }
            try
            {
                e_EuroDizel = Convert.ToDouble(txtedizel.Text);
                if (1000 < depo.EuroDizel + e_EuroDizel || e_EuroDizel <= 0)
                {
                    txtedizel.Text = "Hata ! MAX(1000)";
                }
                else
                {
                    depoBilgileri[3] = Convert.ToString(depo.EuroDizel + e_EuroDizel);
                }
            }
            catch (Exception)
            {

              
            }
            try
            {
                e_Lpg = Convert.ToDouble(txtlpg.Text);
                if (1000 < depo.Lpg + e_Lpg || e_Lpg <= 0)
                {
                    txtlpg.Text = "Hata ! MAX(1000)";
                }
               
                else
                {
                    depoBilgileri[4] = Convert.ToString(depo.Lpg + e_Lpg);
                }
            }
            catch (Exception)
            {

                
            }
            //Eklenen yakıt bilgilerini tekrar txt dosyasına yazdık ve ardından yeni depo bilgilerini okuyup forma yazdık
            System.IO.File.WriteAllLines(Application.StartupPath + "\\depo.txt", depoBilgileri);
            txt_Depo_Oku();
            txt_Depo_Yaz();
            Progresbar_Guncelle();
            Numeric_Value();
        }
        
        private void txt_Depo_Oku()
        {
            //Txt Dosyasındaki depo bilgilerini okuduk ve depo classındaki field alanlarına aktardık.
            depoBilgileri = System.IO.File.ReadAllLines(Application.StartupPath + "\\depo.txt");
            depo.Benzin95 = Convert.ToDouble(depoBilgileri[0]);
            depo.Benzin97 = Convert.ToDouble(depoBilgileri[1]);
            depo.Dizel = Convert.ToDouble(depoBilgileri[2]);
            depo.EuroDizel = Convert.ToDouble(depoBilgileri[3]);
            depo.Lpg = Convert.ToDouble(depoBilgileri[4]);
        }
        private void txt_Depo_Yaz()
        {
            //Txt dosyasından okuyup field lara aktardığımız depo bilgilerini label nesnelerine aktardık
            benzin95.Text = depo.Benzin95.ToString("N")+" lt";
            benzin97.Text = depo.Benzin97.ToString("N") + " lt";
            dizel.Text = depo.Dizel.ToString("N") + " lt"; 
            edizel.Text= depo.EuroDizel.ToString("N") + " lt";
            lpg.Text=depo.Lpg.ToString("N") + " lt";
        }
        private void txt_fiyat_Oku()
        {
            //Txt Dosyasındaki fiyat bilgilerini okuduk ve fiyat classındaki field alanlarına aktardık.
            fiyatBilgileri = System.IO.File.ReadAllLines(Application.StartupPath + "\\fiyat.txt");
            fiyat.Benzin95 = Convert.ToDouble(fiyatBilgileri[0]);
            fiyat.Benzin97 = Convert.ToDouble(fiyatBilgileri[1]);
            fiyat.Dizel = Convert.ToDouble(fiyatBilgileri[2]);
            fiyat.EuroDizel = Convert.ToDouble(fiyatBilgileri[3]);
            fiyat.Lpg = Convert.ToDouble(fiyatBilgileri[4]);
        }
        private void txt_fiyat_Yaz()
        {
            //Txt dosyasından okuyup field lara aktardığımız fiyat bilgilerini label nesnelerine aktardık
            fbenzin95.Text = fiyat.Benzin95.ToString("N") + " ₺";
            fbenzin97.Text = fiyat.Benzin97.ToString("N") + " ₺";
            fdizel.Text = fiyat.Dizel.ToString("N") + " ₺";
            fedizel.Text = fiyat.EuroDizel.ToString("N") + " ₺";
            flpg.Text = fiyat.Lpg.ToString("N") + " ₺";
        }
        
        private void Progresbar_Guncelle()
        {
            //Progressbar değerlerini depo bilgilerine göre güncelledik
            progressBar1.Value = Convert.ToInt16(depo.Benzin95);
            progressBar2.Value = Convert.ToInt16(depo.Benzin97);
            progressBar3.Value = Convert.ToInt16(depo.Dizel);
            progressBar4.Value = Convert.ToInt16(depo.EuroDizel);
            progressBar5.Value = Convert.ToInt16(depo.Lpg);
            
        }
        private void Progresbar_MAX()
        {
            //progressbarların maximum kapasitesini belirttik
            progressBar1.Maximum =1000;
            progressBar2.Maximum =1000;
            progressBar3.Maximum =1000;
            progressBar4.Maximum =1000;
            progressBar5.Maximum =1000;
           
        }
        private void Numeric_Value()
        {
            //Numeric updown nesneleri doldurarak satış yaptığımız için depodaki max litre kadar satış yapmayı sağladık.
            //örneğin 95 oktanlık benzinden depo bilgisinde 20Lt kaldığını varsayarsak numeric nesnesinin değeri max 20 olabilir.
            bn95.Maximum = decimal.Parse(depo.Benzin95.ToString());
            bn97.Maximum = decimal.Parse(depo.Benzin97.ToString());
            bndizel.Maximum = decimal.Parse(depo.Dizel.ToString());
            bnedizel.Maximum = decimal.Parse(depo.EuroDizel.ToString());
            bnLPG.Maximum = decimal.Parse(depo.Lpg.ToString());
        }
        private void Numeric_Counter()
        {
            //Numeric updown nesnesindeki değerlerin kaçar kaçar arttığını ayarladık
            bn95.Increment = 0.5M;
            bn97.Increment = 0.5M;
            bndizel.Increment = 0.5M;
            bnedizel.Increment = 0.5M;
            bnLPG.Increment = 0.5M;
        }
        private void Numeric_Decimal()
        {
            //numeric nesnelerin decimal değerlerde virgülden sonra kaç basamak alabildiğini belirttik
            bn95.DecimalPlaces = 2;
            bn97.DecimalPlaces = 2;
            bndizel.DecimalPlaces = 2;
            bnedizel.DecimalPlaces = 2;
            bnLPG.DecimalPlaces = 2;
        }
        private void Numeric_ReadOnly()
        {
            //Dışardan elle veri girilmesini önledik sadece updown butonlarıyla işlem yapmayı sağladık
            bn95.ReadOnly = true;
            bn97.ReadOnly = true;
            bndizel.ReadOnly = true;
            bnedizel.ReadOnly = true;
            bnLPG.ReadOnly = true;
        }
        private void Numeric_Off()
        {
            //en başta yakıt tipi combo box boş geldiği için tüm numeric updown nesnelerini pasif hale getirdik..
            bn95.Enabled = false;
            bn97.Enabled = false;
            bndizel.Enabled = false;
            bnedizel.Enabled = false;
            bnLPG.Enabled = false;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            //Yazmış olduğumuz tüm metotları Form1 in Load eventine yani form çalıştırıldığında çalışmasını sağladık
            cboxyt.Items.AddRange(YakitTurleri);

            Progresbar_MAX();
            txt_Depo_Oku();
            txt_Depo_Yaz();
            txt_fiyat_Oku();
            txt_fiyat_Yaz();
            Progresbar_Guncelle();
            Numeric_Value();
            Numeric_Counter();
            Numeric_Decimal();
            Numeric_ReadOnly();
            Numeric_Off();

        }
    }
}
