﻿namespace BenzinIstasyonu
{
    partial class Form1
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnb95 = new System.Windows.Forms.Button();
            this.progressBar5 = new System.Windows.Forms.ProgressBar();
            this.progressBar4 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.txtlpg = new System.Windows.Forms.TextBox();
            this.txtedizel = new System.Windows.Forms.TextBox();
            this.txtdizel = new System.Windows.Forms.TextBox();
            this.txtbnz97 = new System.Windows.Forms.TextBox();
            this.txtbnz95 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lpg = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.edizel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dizel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.benzin97 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.benzin95 = new System.Windows.Forms.Label();
            this.txtfedizel = new System.Windows.Forms.TabPage();
            this.btnflpg = new System.Windows.Forms.Button();
            this.txtflpg = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.txtfdizel = new System.Windows.Forms.TextBox();
            this.txtfb97 = new System.Windows.Forms.TextBox();
            this.txtfb95 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.flpg = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.fedizel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.fdizel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.fbenzin97 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.fbenzin95 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button6 = new System.Windows.Forms.Button();
            this.cboxyt = new System.Windows.Forms.ComboBox();
            this.bnLPG = new System.Windows.Forms.NumericUpDown();
            this.bnedizel = new System.Windows.Forms.NumericUpDown();
            this.bndizel = new System.Windows.Forms.NumericUpDown();
            this.bn97 = new System.Windows.Forms.NumericUpDown();
            this.bn95 = new System.Windows.Forms.NumericUpDown();
            this.tutar = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.txtfedizel.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnLPG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnedizel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndizel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bn97)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bn95)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.txtfedizel);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(881, 349);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.btnb95);
            this.tabPage1.Controls.Add(this.progressBar5);
            this.tabPage1.Controls.Add(this.progressBar4);
            this.tabPage1.Controls.Add(this.progressBar3);
            this.tabPage1.Controls.Add(this.progressBar2);
            this.tabPage1.Controls.Add(this.progressBar1);
            this.tabPage1.Controls.Add(this.txtlpg);
            this.tabPage1.Controls.Add(this.txtedizel);
            this.tabPage1.Controls.Add(this.txtdizel);
            this.tabPage1.Controls.Add(this.txtbnz97);
            this.tabPage1.Controls.Add(this.txtbnz95);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.lpg);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.edizel);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.dizel);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.benzin97);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.benzin95);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(873, 323);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Depo Bilgileri";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnb95
            // 
            this.btnb95.Location = new System.Drawing.Point(556, 223);
            this.btnb95.Name = "btnb95";
            this.btnb95.Size = new System.Drawing.Size(211, 41);
            this.btnb95.TabIndex = 3;
            this.btnb95.Text = "Güncelle";
            this.btnb95.UseVisualStyleBackColor = true;
            this.btnb95.Click += new System.EventHandler(this.btnb95_Click);
            // 
            // progressBar5
            // 
            this.progressBar5.Location = new System.Drawing.Point(556, 189);
            this.progressBar5.Name = "progressBar5";
            this.progressBar5.Size = new System.Drawing.Size(212, 23);
            this.progressBar5.TabIndex = 2;
            // 
            // progressBar4
            // 
            this.progressBar4.Location = new System.Drawing.Point(556, 155);
            this.progressBar4.Name = "progressBar4";
            this.progressBar4.Size = new System.Drawing.Size(212, 23);
            this.progressBar4.TabIndex = 2;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(556, 121);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(212, 23);
            this.progressBar3.TabIndex = 2;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(556, 87);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(212, 23);
            this.progressBar2.TabIndex = 2;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(556, 53);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(212, 23);
            this.progressBar1.TabIndex = 2;
            // 
            // txtlpg
            // 
            this.txtlpg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtlpg.Location = new System.Drawing.Point(306, 188);
            this.txtlpg.Name = "txtlpg";
            this.txtlpg.Size = new System.Drawing.Size(181, 29);
            this.txtlpg.TabIndex = 1;
            // 
            // txtedizel
            // 
            this.txtedizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtedizel.Location = new System.Drawing.Point(306, 154);
            this.txtedizel.Name = "txtedizel";
            this.txtedizel.Size = new System.Drawing.Size(181, 29);
            this.txtedizel.TabIndex = 1;
            // 
            // txtdizel
            // 
            this.txtdizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtdizel.Location = new System.Drawing.Point(306, 120);
            this.txtdizel.Name = "txtdizel";
            this.txtdizel.Size = new System.Drawing.Size(181, 29);
            this.txtdizel.TabIndex = 1;
            // 
            // txtbnz97
            // 
            this.txtbnz97.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtbnz97.Location = new System.Drawing.Point(306, 87);
            this.txtbnz97.Name = "txtbnz97";
            this.txtbnz97.Size = new System.Drawing.Size(181, 29);
            this.txtbnz97.TabIndex = 1;
            // 
            // txtbnz95
            // 
            this.txtbnz95.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtbnz95.Location = new System.Drawing.Point(306, 53);
            this.txtbnz95.Name = "txtbnz95";
            this.txtbnz95.Size = new System.Drawing.Size(181, 29);
            this.txtbnz95.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label5.Location = new System.Drawing.Point(46, 188);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "LPG";
            // 
            // lpg
            // 
            this.lpg.AutoSize = true;
            this.lpg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lpg.Location = new System.Drawing.Point(179, 188);
            this.lpg.Name = "lpg";
            this.lpg.Size = new System.Drawing.Size(64, 24);
            this.lpg.TabIndex = 0;
            this.lpg.Text = "Value";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label4.Location = new System.Drawing.Point(46, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Euro Dizel";
            // 
            // edizel
            // 
            this.edizel.AutoSize = true;
            this.edizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.edizel.Location = new System.Drawing.Point(179, 154);
            this.edizel.Name = "edizel";
            this.edizel.Size = new System.Drawing.Size(64, 24);
            this.edizel.TabIndex = 0;
            this.edizel.Text = "Value";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(46, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Dizel";
            // 
            // dizel
            // 
            this.dizel.AutoSize = true;
            this.dizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dizel.Location = new System.Drawing.Point(179, 120);
            this.dizel.Name = "dizel";
            this.dizel.Size = new System.Drawing.Size(64, 24);
            this.dizel.TabIndex = 0;
            this.dizel.Text = "Value";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(46, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Benzin97";
            // 
            // benzin97
            // 
            this.benzin97.AutoSize = true;
            this.benzin97.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.benzin97.Location = new System.Drawing.Point(179, 86);
            this.benzin97.Name = "benzin97";
            this.benzin97.Size = new System.Drawing.Size(64, 24);
            this.benzin97.TabIndex = 0;
            this.benzin97.Text = "Value";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(46, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Benzin95";
            // 
            // benzin95
            // 
            this.benzin95.AutoSize = true;
            this.benzin95.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.benzin95.Location = new System.Drawing.Point(179, 52);
            this.benzin95.Name = "benzin95";
            this.benzin95.Size = new System.Drawing.Size(64, 24);
            this.benzin95.TabIndex = 0;
            this.benzin95.Text = "Value";
            // 
            // txtfedizel
            // 
            this.txtfedizel.Controls.Add(this.btnflpg);
            this.txtfedizel.Controls.Add(this.txtflpg);
            this.txtfedizel.Controls.Add(this.textBox2);
            this.txtfedizel.Controls.Add(this.txtfdizel);
            this.txtfedizel.Controls.Add(this.txtfb97);
            this.txtfedizel.Controls.Add(this.txtfb95);
            this.txtfedizel.Controls.Add(this.label6);
            this.txtfedizel.Controls.Add(this.flpg);
            this.txtfedizel.Controls.Add(this.label8);
            this.txtfedizel.Controls.Add(this.fedizel);
            this.txtfedizel.Controls.Add(this.label10);
            this.txtfedizel.Controls.Add(this.fdizel);
            this.txtfedizel.Controls.Add(this.label12);
            this.txtfedizel.Controls.Add(this.fbenzin97);
            this.txtfedizel.Controls.Add(this.label16);
            this.txtfedizel.Controls.Add(this.label14);
            this.txtfedizel.Controls.Add(this.fbenzin95);
            this.txtfedizel.Location = new System.Drawing.Point(4, 22);
            this.txtfedizel.Name = "txtfedizel";
            this.txtfedizel.Padding = new System.Windows.Forms.Padding(3);
            this.txtfedizel.Size = new System.Drawing.Size(794, 271);
            this.txtfedizel.TabIndex = 1;
            this.txtfedizel.Text = "Fiyat Bilgileri";
            this.txtfedizel.UseVisualStyleBackColor = true;
            // 
            // btnflpg
            // 
            this.btnflpg.Location = new System.Drawing.Point(511, 204);
            this.btnflpg.Name = "btnflpg";
            this.btnflpg.Size = new System.Drawing.Size(131, 39);
            this.btnflpg.TabIndex = 23;
            this.btnflpg.Text = "Güncelle";
            this.btnflpg.UseVisualStyleBackColor = true;
            this.btnflpg.Click += new System.EventHandler(this.btnflpg_Click);
            // 
            // txtflpg
            // 
            this.txtflpg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtflpg.Location = new System.Drawing.Point(380, 214);
            this.txtflpg.Name = "txtflpg";
            this.txtflpg.Size = new System.Drawing.Size(66, 29);
            this.txtflpg.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.textBox2.Location = new System.Drawing.Point(380, 177);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(66, 29);
            this.textBox2.TabIndex = 4;
            // 
            // txtfdizel
            // 
            this.txtfdizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtfdizel.Location = new System.Drawing.Point(380, 145);
            this.txtfdizel.Name = "txtfdizel";
            this.txtfdizel.Size = new System.Drawing.Size(66, 29);
            this.txtfdizel.TabIndex = 3;
            // 
            // txtfb97
            // 
            this.txtfb97.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtfb97.Location = new System.Drawing.Point(380, 110);
            this.txtfb97.Name = "txtfb97";
            this.txtfb97.Size = new System.Drawing.Size(66, 29);
            this.txtfb97.TabIndex = 2;
            // 
            // txtfb95
            // 
            this.txtfb95.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txtfb95.Location = new System.Drawing.Point(380, 75);
            this.txtfb95.Name = "txtfb95";
            this.txtfb95.Size = new System.Drawing.Size(66, 29);
            this.txtfb95.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label6.Location = new System.Drawing.Point(156, 214);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "LPG";
            // 
            // flpg
            // 
            this.flpg.AutoSize = true;
            this.flpg.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.flpg.Location = new System.Drawing.Point(289, 214);
            this.flpg.Name = "flpg";
            this.flpg.Size = new System.Drawing.Size(64, 24);
            this.flpg.TabIndex = 11;
            this.flpg.Text = "Value";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label8.Location = new System.Drawing.Point(156, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(107, 24);
            this.label8.TabIndex = 10;
            this.label8.Text = "Euro Dizel";
            // 
            // fedizel
            // 
            this.fedizel.AutoSize = true;
            this.fedizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.fedizel.Location = new System.Drawing.Point(289, 180);
            this.fedizel.Name = "fedizel";
            this.fedizel.Size = new System.Drawing.Size(64, 24);
            this.fedizel.TabIndex = 9;
            this.fedizel.Text = "Value";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label10.Location = new System.Drawing.Point(156, 146);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 24);
            this.label10.TabIndex = 8;
            this.label10.Text = "Dizel";
            // 
            // fdizel
            // 
            this.fdizel.AutoSize = true;
            this.fdizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.fdizel.Location = new System.Drawing.Point(289, 146);
            this.fdizel.Name = "fdizel";
            this.fdizel.Size = new System.Drawing.Size(64, 24);
            this.fdizel.TabIndex = 7;
            this.fdizel.Text = "Value";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label12.Location = new System.Drawing.Point(156, 112);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 24);
            this.label12.TabIndex = 6;
            this.label12.Text = "Benzin97";
            // 
            // fbenzin97
            // 
            this.fbenzin97.AutoSize = true;
            this.fbenzin97.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.fbenzin97.Location = new System.Drawing.Point(289, 112);
            this.fbenzin97.Name = "fbenzin97";
            this.fbenzin97.Size = new System.Drawing.Size(64, 24);
            this.fbenzin97.TabIndex = 5;
            this.fbenzin97.Text = "Value";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label16.Location = new System.Drawing.Point(156, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(403, 24);
            this.label16.TabIndex = 13;
            this.label16.Text = "% Zam için + İndirim için - Değer Girilebilir";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label14.Location = new System.Drawing.Point(156, 78);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 24);
            this.label14.TabIndex = 13;
            this.label14.Text = "Benzin95";
            // 
            // fbenzin95
            // 
            this.fbenzin95.AutoSize = true;
            this.fbenzin95.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.fbenzin95.Location = new System.Drawing.Point(289, 78);
            this.fbenzin95.Name = "fbenzin95";
            this.fbenzin95.Size = new System.Drawing.Size(64, 24);
            this.fbenzin95.TabIndex = 4;
            this.fbenzin95.Text = "Value";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button6);
            this.tabPage3.Controls.Add(this.cboxyt);
            this.tabPage3.Controls.Add(this.bnLPG);
            this.tabPage3.Controls.Add(this.bnedizel);
            this.tabPage3.Controls.Add(this.bndizel);
            this.tabPage3.Controls.Add(this.bn97);
            this.tabPage3.Controls.Add(this.bn95);
            this.tabPage3.Controls.Add(this.tutar);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label17);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(794, 271);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Satış Yap";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button6.Location = new System.Drawing.Point(341, 52);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(123, 162);
            this.button6.TabIndex = 21;
            this.button6.Text = "Satış Yap";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // cboxyt
            // 
            this.cboxyt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxyt.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cboxyt.FormattingEnabled = true;
            this.cboxyt.Location = new System.Drawing.Point(215, 14);
            this.cboxyt.Name = "cboxyt";
            this.cboxyt.Size = new System.Drawing.Size(249, 32);
            this.cboxyt.TabIndex = 20;
            this.cboxyt.SelectedIndexChanged += new System.EventHandler(this.cboxyt_SelectedIndexChanged);
            // 
            // bnLPG
            // 
            this.bnLPG.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bnLPG.Location = new System.Drawing.Point(215, 185);
            this.bnLPG.Name = "bnLPG";
            this.bnLPG.Size = new System.Drawing.Size(120, 29);
            this.bnLPG.TabIndex = 19;
            // 
            // bnedizel
            // 
            this.bnedizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bnedizel.Location = new System.Drawing.Point(215, 151);
            this.bnedizel.Name = "bnedizel";
            this.bnedizel.Size = new System.Drawing.Size(120, 29);
            this.bnedizel.TabIndex = 19;
            // 
            // bndizel
            // 
            this.bndizel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bndizel.Location = new System.Drawing.Point(215, 117);
            this.bndizel.Name = "bndizel";
            this.bndizel.Size = new System.Drawing.Size(120, 29);
            this.bndizel.TabIndex = 19;
            // 
            // bn97
            // 
            this.bn97.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bn97.Location = new System.Drawing.Point(215, 83);
            this.bn97.Name = "bn97";
            this.bn97.Size = new System.Drawing.Size(120, 29);
            this.bn97.TabIndex = 19;
            // 
            // bn95
            // 
            this.bn95.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.bn95.Location = new System.Drawing.Point(215, 49);
            this.bn95.Name = "bn95";
            this.bn95.Size = new System.Drawing.Size(120, 29);
            this.bn95.TabIndex = 19;
            // 
            // tutar
            // 
            this.tutar.AutoSize = true;
            this.tutar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.tutar.Location = new System.Drawing.Point(271, 221);
            this.tutar.Name = "tutar";
            this.tutar.Size = new System.Drawing.Size(64, 24);
            this.tutar.TabIndex = 17;
            this.tutar.Text = "Value";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label18.Location = new System.Drawing.Point(98, 221);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(167, 24);
            this.label18.TabIndex = 17;
            this.label18.Text = "Ödenecek Tutar:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label7.Location = new System.Drawing.Point(98, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 24);
            this.label7.TabIndex = 17;
            this.label7.Text = "LPG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label9.Location = new System.Drawing.Point(98, 153);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(107, 24);
            this.label9.TabIndex = 16;
            this.label9.Text = "Euro Dizel";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label11.Location = new System.Drawing.Point(98, 119);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 24);
            this.label11.TabIndex = 15;
            this.label11.Text = "Dizel";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label13.Location = new System.Drawing.Point(98, 85);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 24);
            this.label13.TabIndex = 14;
            this.label13.Text = "Benzin97";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label17.Location = new System.Drawing.Point(98, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(104, 24);
            this.label17.TabIndex = 18;
            this.label17.Text = "Yakıt Türü";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label15.Location = new System.Drawing.Point(98, 51);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 24);
            this.label15.TabIndex = 18;
            this.label15.Text = "Benzin95";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label19.Location = new System.Drawing.Point(256, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(305, 24);
            this.label19.TabIndex = 14;
            this.label19.Text = "Akaryakıt Depo Doluluk Oranları";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label20.Location = new System.Drawing.Point(20, 284);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(265, 24);
            this.label20.TabIndex = 0;
            this.label20.Text = "Depo Kapasitesi:Max 1000lt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(881, 349);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Benzin Istasyonu";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.txtfedizel.ResumeLayout(false);
            this.txtfedizel.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bnLPG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bnedizel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndizel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bn97)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bn95)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage txtfedizel;
        private System.Windows.Forms.Button btnb95;
        private System.Windows.Forms.ProgressBar progressBar5;
        private System.Windows.Forms.ProgressBar progressBar4;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox txtlpg;
        private System.Windows.Forms.TextBox txtedizel;
        private System.Windows.Forms.TextBox txtdizel;
        private System.Windows.Forms.TextBox txtbnz97;
        private System.Windows.Forms.TextBox txtbnz95;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lpg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label edizel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label dizel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label benzin97;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label benzin95;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtflpg;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox txtfdizel;
        private System.Windows.Forms.TextBox txtfb97;
        private System.Windows.Forms.TextBox txtfb95;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label flpg;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label fedizel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label fdizel;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label fbenzin97;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label fbenzin95;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ComboBox cboxyt;
        private System.Windows.Forms.NumericUpDown bnLPG;
        private System.Windows.Forms.NumericUpDown bnedizel;
        private System.Windows.Forms.NumericUpDown bndizel;
        private System.Windows.Forms.NumericUpDown bn97;
        private System.Windows.Forms.NumericUpDown bn95;
        private System.Windows.Forms.Label tutar;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnflpg;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
    }
}

